FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

ENV DEBIAN_FRONTEND noninteractive

ENV NODE_VERSION=v18.12.1 \
    CLOJURE_VERSION=1.11.1.1189 \
    CLJKONDO_VERSION=2022.10.14 \
    BABASHKA_VERSION=0.9.162 \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8

ENV VERSION=1.18.5
ENV CURRENT_VERSION=$VERSION
#ENV CURRENT_HASH=${CURRENT_HASH:-$(git rev-parse --short HEAD)};
#ENV CURRENT_HASH=24fa4f71a

RUN export BUILD_DATE=$(date -R)
ENV SHADOWCLJS_EXTRA_PARAMS=""
ENV EXTRA_PARAMS=$SHADOWCLJS_EXTRA_PARAMS

#ENV NODE_ENV=production

RUN mkdir -p /app/code /app/data /app/pkg/penpot

RUN : \
    && apt-get -qq update \
    && apt-get -qqy install --no-install-recommends \
        build-essential \
        imagemagick \
        ghostscript \
        netpbm \
        poppler-utils \
        potrace \
        webp \
        nginx \
        jq \
        redis-tools \
        woff-tools \
        woff2 \
        fontforge \
        openssh-client \
    && rm -rf /var/lib/apt/lists/* \
	&& :


RUN : \
    && apt-get -qq update \
    && apt-get -qqy install \
        gconf-service \
        libasound2 \
        libatk1.0-0 \
        libatk-bridge2.0-0 \
        libcairo2 \
        libcups2 \
        libdbus-1-3 \
        libexpat1 \
        libfontconfig1 \
        libgcc1 \
        libgconf-2-4 \
        libgdk-pixbuf2.0-0 \
        libglib2.0-0 \
        libgtk-3-0 \
        libnspr4 \
        libpango-1.0-0 \
        libpangocairo-1.0-0 \
        libx11-6 \
        libx11-xcb1 \
        libxcb1 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxfixes3 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxshmfence1 \
        libxss1 \
        libxtst6 \
        fonts-liberation \
        libappindicator1 \
        libnss3 \
        libgbm1 \
        xvfb \
    && rm -rf /var/lib/apt/lists/* \
	&& :

# install openjdk
RUN : \
	&& curl -LfsSo /tmp/openjdk.tar.gz https://cdn.azul.com/zulu/bin/zulu19.30.11-ca-jdk19.0.1-linux_x64.tar.gz \
	&& echo "2ac8cd9e7e1e30c8fba107164a2ded9fad698326899564af4b1254815adfaa8a */tmp/openjdk.tar.gz" | sha256sum -c - \
	&& mkdir -p /usr/lib/jvm/openjdk \
	&& cd /usr/lib/jvm/openjdk \
	&& tar -xf /tmp/openjdk.tar.gz --strip-components=1 \
	&& rm -rf /tmp/openjdk.tar.gz \
	&& :
ENV PATH="/usr/lib/jvm/openjdk/bin:/usr/local/nodejs/bin:$PATH" JAVA_HOME=/usr/lib/jvm/openjdk


RUN : \
	&& curl -LfsSo /tmp/clojure.sh https://download.clojure.org/install/linux-install-$CLOJURE_VERSION.sh \
	&& chmod +x /tmp/clojure.sh \
	&& /tmp/clojure.sh \
	&& rm -rf /tmp/clojure.sh \
	&& :

# Install clj-kondo (Linter)
#RUN curl -LfsSo /tmp/clj-kondo.zip https://github.com/borkdude/clj-kondo/releases/download/v$CLJKONDO_VERSION/clj-kondo-$CLJKONDO_VERSION-linux-amd64.zip \
#    && cd /usr/local/bin \
#    && unzip /tmp/clj-kondo.zip \
#    && rm /tmp/clj-kondo.zip

# install babashka (bb) to build backend
RUN : \
	&& curl -LfsSo /tmp/babashka.tar.gz https://github.com/babashka/babashka/releases/download/v$BABASHKA_VERSION/babashka-$BABASHKA_VERSION-linux-amd64.tar.gz \
	&& cd /usr/local/bin \
	&& tar -xf /tmp/babashka.tar.gz \
	&& rm -rf /tmp/babashka.tar.gz \
	&& :

RUN wget https://github.com/penpot/penpot/archive/refs/tags/${VERSION}.tar.gz -O - | \
	tar -xz --strip-components 1 -C /app/pkg/penpot

# build frontend bundle (see ./manage.sh, frontend/scripts/build) 
WORKDIR /app/pkg/penpot/frontend
RUN : \
	&& yarn install \
	&& npx gulp clean \
#	&& clojure -J-Xms100M -J-Xmx800M -J-XX:+UseSerialGC -M:dev:shadow-cljs release main --config-merge "{:release-version \"${CURRENT_HASH}\"}" $EXTRA_PARAMS \
	&& clojure -J-Xms100M -J-Xmx800M -J-XX:+UseSerialGC -M:dev:shadow-cljs release main $EXTRA_PARAMS \
	&& mkdir -p target/dist \
	&& npx gulp build \
	&& npx gulp dist:clean \
	&& npx gulp dist:copy \
	&& sed -i -re "s/\%version\%/$VERSION/g" ./target/dist/index.html \
	&& sed -i -re "s/\%buildDate\%/$BUILD_DATE/g" ./target/dist/index.html \
	&& mv ./target/dist /app/code/frontend \
	&& echo $VERSION > /app/code/frontend/version.txt \
	&& :

RUN : \
	&& mkdir -p /app/data/frontend/js \
	&& ln -sf /app/data/frontend/js/config.js /app/code/frontend/js/config.js \
	&& :

# build exporter
WORKDIR /app/pkg/penpot/exporter
RUN : \
	&& yarn install \
	&& clojure -M:dev:shadow-cljs release main \
	&& rm -rf target/app \
	&& cp yarn.lock target/ \
	&& cp package.json target/ \
	&& sed -i -re "s/\%version\%/$CURRENT_VERSION/g" ./target/app.js \
	&& mv ./target /app/code/exporter \
	&& echo $VERSION > /app/code/exporter/version.txt \
	&& :
WORKDIR /app/code/exporter
RUN : \
	&& yarn \
	&& yarn run playwright install chromium \
	&& :

# build backend bundle (see /manage.sh, backend/scripts/build)
WORKDIR /app/pkg/penpot/backend
RUN : \
	&& mkdir -p target/classes \
	&& mkdir -p target/dist \
	&& echo "$CURRENT_VERSION" > target/classes/version.txt \
	&& cp ../CHANGES.md target/classes/changelog.md \
	&& clojure -T:build jar \
	&& mv target/penpot.jar target/dist/penpot.jar \
	&& cp resources/log4j2.xml target/dist/log4j2.xml \
#	&& cp scripts/run.template.sh target/dist/run.sh \
	&& cp scripts/manage.py target/dist/manage.py \
#	&& chmod +x target/dist/run.sh \
	&& chmod +x target/dist/manage.py \
	&& bb ./scripts/prefetch-templates.clj resources/app/onboarding.edn builtin-templates/ \
	&& cp -r builtin-templates target/dist/ \
	&& mv ./target/dist /app/code/backend \
	&& echo $VERSION > /app/code/backend/version.txt \
	&& ls -la /app/code/backend \
	&& :

WORKDIR /app/code

# add nginx config
RUN : \
	&& rm /etc/nginx/sites-enabled/* \
	&& ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log \
	&& rm -rf /var/lib/nginx/ \
	&& mkdir -p /run/nginx/tmp \
	&& ln -sf /run/nginx /var/lib/nginx \
	&& :

COPY conf/nginx/penpot-nginx.conf /etc/nginx/nginx.conf

# Add supervisor configs
COPY conf/supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/penpot/supervisord.log /var/log/supervisor/supervisord.log

COPY start.sh /app/pkg/
RUN chmod a+x /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
