### Overview

Penpot is the first Open Source design and prototyping platform meant for cross-domain teams.
Non dependent on operating systems, Penpot is web based and works with open standards (SVG).
Penpot invites designers all over the world to fall in love with open source while getting developers excited about the design process in return.
