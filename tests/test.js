#!/usr/bin/env node

/* jshint esversion: 8 */
/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var LOCATION = 'test';
    var EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    var TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    var browser, app, shareLink;
    var username = process.env.EMAIL;
    var password = process.env.PASSWORD;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();

        await browser.sleep(2000);

        await browser.get(`https://${app.fqdn}/#/auth/login`);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//a[contains(@class, "btn-github-auth") and contains(text(), "OpenID")]')).click();
        await browser.sleep(2000);

        if (await browser.findElements(By.xpath('//input[@name="username"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.findElement(By.xpath('//button[@type="submit" and contains(text(), "Log in")]')).click();
            await browser.sleep(2000);
        }

        if (await browser.findElements(By.xpath('//button[@type="submit" and contains(text(), "Authorize")]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//button[@type="submit" and contains(text(), "Authorize")]')).click();
            await browser.sleep(2000);
        }
        if (await browser.findElements(By.xpath('//input[@type="submit" and @value="Create an account"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//input[@type="submit" and @value="Create an account"]')).click();
            await browser.sleep(2000);
        }
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Projects")]')), TIMEOUT);
    }

    async function getMainPage() {
        await browser.get('https://' + app.fqdn + '/#/dashboard');
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Projects")]')), TIMEOUT);
    }

    async function getProject() {
        await browser.get('https://' + app.fqdn + '/#/dashboard/');
        await browser.sleep(5000);

        while (await browser.findElements(By.xpath('//button[@data-test="onboarding-next-btn"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//button[@data-test="onboarding-next-btn"]')).click();
            await browser.sleep(2000);
        }

//        await browser.findElement(By.xpath('//button[@data-test="onboarding-next-btn"]')).click();
//        await browser.sleep(2000);

        if (await browser.findElements(By.xpath('//div[contains(@class,"newsletter")]//button[@class="btn-primary" and contains(text(), "Continue")]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//div[contains(@class,"newsletter")]//button[@class="btn-primary" and contains(text(), "Continue")]')).click();
            await browser.sleep(2000);
        }

        if (await browser.findElements(By.xpath('//button[@class="skip-action" and contains(text(), "Create a team later")]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//button[@class="skip-action" and contains(text(), "Create a team later")]')).click();
            await browser.sleep(15000);
        }

        await browser.findElement(By.xpath('//button[@data-test="new-project-button"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//h1[contains(text(), "Projects")]')).click();
        await browser.sleep(2000);
        await browser.get('https://' + app.fqdn + '/#/dashboard/');
        await browser.sleep(2000);

    }

    async function checkProject() {
        await browser.get('https://' + app.fqdn + '/#/dashboard/');
        await browser.sleep(5000);
        await browser.wait(until.elementLocated(By.xpath('//div[@class="project-name-wrapper"]/h2[contains(text(), "New Project 1")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () {
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('can create project', getProject);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('can login', login.bind(null, username, password));

    it('check Project', checkProject);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('check Project', checkProject);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);
    it('check Project', checkProject);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
/*
    it('can install app', async function () {
        execSync(`cloudron install --appstore-id app.penpot.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
        // wait when all services are up and running
        await sleep(20000);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can get the main page', getMainPage);

    it('can create project', getProject);
    it('check Project', checkProject);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('check Project', checkProject);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
*/
});

