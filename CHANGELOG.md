[0.1.0]
* Initial version for Penpot

[0.2.0]
* Update Penpot to  1.18.4
* [Full changelog](https://github.com/penpot/penpot/releases/tag/1.18.4)
* Fix zooming while color picker breaks UI GH #3214
* Fix problem with layout not reflowing on shape deletion Taiga #5289
* Fix extra long typography names on assets and palette Taiga #5199
* Fix background-color property on inspect code Taiga #5300
* Preview layer blend modes (by @akshay-gupta7) Github #3235

[0.3.0]
* Improve healthcheck

[0.4.0]
* Update Penpot to 1.18.5
* [Full changelog](https://github.com/penpot/penpot/releases/tag/1.18.5)
* Fix add flow option in contextual menu for frames

